# socialHub

## About
<b>socialHub</b> is an app for public communication between people.<br />
You can write/edit/delete posts, put likes or dislikes, attach images and videos, make check-ins using Google Maps, 
chat in public room with others, search for post/people, upload/delete profile images and so on.
 
 ## Usage
 This is **Spring Boot 2** application build with [Spring initializr](https://start.spring.io/) using 
 Maven project structure and Java 11 running on localhost with Apache TomCat server. <br>Other technologies used:
 - Spring Boot Security 2.4.0
 - Thymeleaf
 - MySQL
 - Lombok
 - Hibernate JPA
 - Hibernate Search ORM 5.11.7
 - JavaMail API
 - WebSocket API
 - JavaScript
 - AJAX
 - Google Maps API
 
 ## How to run
 1. clone or download project<br>
 2. create a database named db_socialhub on your local machine <br>
 3. in application.properties edit:<br>
   -database server name and password<br>
   -upload.path for a file uploading directory on your local machine<br>
   -localhost port (default is 8081)<br>
 4. run SocialhubApplication.java<br>
 5. in any browser type localhost:8081 and press ENTER
  
 ## Visuals
 ![pic1](socialhub-1.PNG)

 ![pic2](socialhub-2.PNG)
 
 ![pic3](socialhub-3.PNG)

 ![pic4](socialhub-4.PNG)