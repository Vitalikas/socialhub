package lt.kolinko.socialhub.controllers;

import lt.kolinko.socialhub.dto.*;
import lt.kolinko.socialhub.services.CommentService;
import lt.kolinko.socialhub.services.MessageService;
import lt.kolinko.socialhub.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import java.security.Principal;

@Controller
public class CommentController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private UserService userService;
    @Value("${upload.path}")
    private String uploadPath;

    @GetMapping("/posts/comments")
    public String showComments(Model model) {
        Iterable<CommentDto> comments = commentService.showAll();
        model.addAttribute("comments", comments);
        return "main/main";
    }

    @PostMapping("/posts/comments/new")
    public String writeComment(Principal principal,
                               @ModelAttribute("comment") @Valid CommentCreateDto commentCreateDto,
                               BindingResult bindingResult,
                               @RequestParam("postId") Long postId,
                               @RequestParam("file") MultipartFile file) {
        if (bindingResult.hasErrors()) {
            return "main/main";
        }

        // saving comment
        commentService.save(postId, commentCreateDto, file, principal);
        return "redirect:/";
    }
}
