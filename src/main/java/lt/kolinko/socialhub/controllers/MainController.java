package lt.kolinko.socialhub.controllers;

import lt.kolinko.socialhub.dto.CommentDto;
import lt.kolinko.socialhub.dto.MessageDto;
import lt.kolinko.socialhub.dto.UserDto;
import lt.kolinko.socialhub.services.MessageService;
import lt.kolinko.socialhub.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.security.Principal;
import java.util.Collections;
import java.util.List;

@Controller
public class MainController {
    @Autowired
    private UserService userService;
    @Autowired
    private MessageService messageService;
    @Value("${upload.path}")
    private String uploadPath;

    @GetMapping("/")
    public String showAllPosts(@RequestParam(value = "text", required = false) String text,
                               @RequestParam(value = "limit", required = false, defaultValue = "100") Integer limit,
                               @RequestParam(value = "offset", required = false, defaultValue = "0") Integer offset,
                               Model model,
                               Principal principal,
                               @ModelAttribute("comment") CommentDto commentDto) {
        Iterable<MessageDto> posts = messageService.searchByText(text, limit, offset);
        Collections.shuffle((List<?>) posts); // show posts in random order
        model.addAttribute("posts", posts);

        UserDto loggedUser = userService.findByEmail(principal.getName());
        model.addAttribute("user", loggedUser);

        return "main/main";
    }

//    @GetMapping("/")
//    public String index() {
//        return "main/main";
//    }
}
