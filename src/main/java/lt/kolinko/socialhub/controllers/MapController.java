package lt.kolinko.socialhub.controllers;

import lt.kolinko.socialhub.utils.LocationAddingUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MapController {
    @Value("${tomtom.apikey}")
    private String tomTomApiKey;

    @GetMapping("/map")
    public String mapPage(Model model) {
        model.addAttribute("apikey", tomTomApiKey);
        model.addAttribute("savedLocations", LocationAddingUtil.savedLocations());
        return "maps/map";
    }

    @GetMapping("/gmap")
    public String gMap() {
        return "maps/googleMaps";
    }

    @GetMapping("/reverse")
    public String reverse() {
        return "maps/reverseGeocoding";
    }
}
