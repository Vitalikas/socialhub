package lt.kolinko.socialhub.controllers;

import lombok.SneakyThrows;
import lt.kolinko.socialhub.dto.*;
import lt.kolinko.socialhub.services.MessageService;
import lt.kolinko.socialhub.services.UserService;
import lt.kolinko.socialhub.utils.FileUploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/posts")
public class MessageController {
    boolean attachedFileDeleted = false;
    @Autowired
    private MessageService messageService;
    @Autowired
    private UserService userService;
    @Value("${upload.path}")
    private String uploadPath;

    @GetMapping("/new")
    public String showNewPostCreationForm(@ModelAttribute("message") MessageCreateDto messageCreateDto,
                                          Principal principal,
                                          Model model) {
        UserDto loggedUser = userService.findByEmail(principal.getName());
        model.addAttribute("user", loggedUser);

        return "posts/new";
    }

    @SneakyThrows
    @PostMapping("/create")
    public String createPost(Principal principal,
                             @ModelAttribute("message") @Valid MessageCreateDto messageCreateDto,
                             BindingResult bindingResult,
                             @RequestParam("files") List<MultipartFile> files) {
        if (bindingResult.hasErrors()) {
            return "posts/new";
        }

        messageService.save(messageCreateDto, files, principal);
        return "redirect:/";
    }

    @PreAuthorize("#postAuthorId == authentication.principal.id")
    @GetMapping("/{id}/edit")
    public String showPostEditForm(@PathVariable("id") Long id,
                                   @RequestParam("postAuthorId") Long postAuthorId,
                                   Model model) {
//        Long loggedUserId = userService.findByEmail(principal.getName()).getId();
//        Long postAuthorId = messageService.findById(id).getAuthor().getId();
        model.addAttribute("message", messageService.findById(id));

        return "posts/edit";
    }

    @SneakyThrows
    @PostMapping(value = "/{id}/edit", params = "Save")
    public String updatePost(@ModelAttribute("message") @Valid MessageEditDto messageEditDto,
                                BindingResult bindingResult,
                                @PathVariable("id") Long id,
                                @RequestParam("files") List<MultipartFile> files) {
        if (bindingResult.hasErrors()) {
            return "posts/edit";
        }

        MessageDto messageDto = messageService.findById(id);

        if (!messageEditDto.getTag().equals(messageDto.getTag()) ||
            !messageEditDto.getText().equals(messageDto.getText()) ||
            attachedFileDeleted) {
            messageEditDto.setEdited(true);
            messageEditDto.setEditTime(LocalDateTime.now());
        }

        String filename1 = FileUploadUtil.getUuidFilename1(files);
        String filename2 = FileUploadUtil.getUuidFilename2(files);
        String filename3 = FileUploadUtil.getUuidFilename3(files);

        if (FileUploadUtil.upload1(uploadPath, filename1, files)) {
            messageEditDto.setFilename1(filename1);
            messageEditDto.setEdited(true);
            messageEditDto.setEditTime(LocalDateTime.now());
        } else {
            messageEditDto.setFilename1(messageService.findById(id).getFilename1());
        }

        if (FileUploadUtil.upload2(uploadPath, filename2, files)) {
            messageEditDto.setFilename2(filename2);
            messageEditDto.setEdited(true);
            messageEditDto.setEditTime(LocalDateTime.now());
        } else {
            messageEditDto.setFilename2(messageService.findById(id).getFilename2());
        }

        if (FileUploadUtil.upload3(uploadPath, filename3, files)) {
            messageEditDto.setFilename3(filename3);
            messageEditDto.setEdited(true);
            messageEditDto.setEditTime(LocalDateTime.now());
        } else {
            messageEditDto.setFilename3(messageService.findById(id).getFilename3());
        }

        messageService.updateById(id, messageEditDto);

        return "redirect:/";
    }

    @PostMapping("/{id}/delete")
    public String deletePost(@PathVariable("id") Long id) {
        messageService.deleteById(id);

        return "redirect:/";
    }

    @PostMapping(value = "/{id}/edit", params = "Delete1")
    public String deleteFile1(@ModelAttribute("message") @Valid MessageEditDto messageEditDto,
                             BindingResult bindingResult,
                             @PathVariable("id") Long id,
                             @RequestParam("files") List<MultipartFile> files) {
        if (bindingResult.hasErrors()) {
            return "posts/edit";
        }

        if (attachedFileDeleted = messageService.deleteAttachedFile1(id)) {
            messageEditDto.setFilename1(null);
            messageEditDto.setEdited(true);
            messageEditDto.setEditTime(LocalDateTime.now());
        }

        String filename2 = FileUploadUtil.getUuidFilename2(files);
        String filename3 = FileUploadUtil.getUuidFilename3(files);

        if (FileUploadUtil.upload2(uploadPath, filename2, files)) {
            messageEditDto.setFilename2(filename2);
        } else {
            messageEditDto.setFilename2(messageService.findById(id).getFilename2());
        }

        if (FileUploadUtil.upload3(uploadPath, filename3, files)) {
            messageEditDto.setFilename3(filename3);
        } else {
            messageEditDto.setFilename3(messageService.findById(id).getFilename3());
        }

        messageService.updateById(id, messageEditDto);

        return "posts/edit";
    }

    @PostMapping(value = "/{id}/edit", params = "Delete2")
    public String deleteFile2(@ModelAttribute("message") @Valid MessageEditDto messageEditDto,
                             BindingResult bindingResult,
                             @PathVariable("id") Long id,
                             @RequestParam("files") List<MultipartFile> files) {
        if (bindingResult.hasErrors()) {
            return "posts/edit";
        }

        if (attachedFileDeleted = messageService.deleteAttachedFile2(id)) {
            messageEditDto.setFilename2(null);
            messageEditDto.setEdited(true);
            messageEditDto.setEditTime(LocalDateTime.now());
        }

        String filename1 = FileUploadUtil.getUuidFilename1(files);
        String filename3 = FileUploadUtil.getUuidFilename3(files);

        if (FileUploadUtil.upload1(uploadPath, filename1, files)) {
            messageEditDto.setFilename1(filename1);
        } else {
            messageEditDto.setFilename1(messageService.findById(id).getFilename1());
        }

        if (FileUploadUtil.upload3(uploadPath, filename3, files)) {
            messageEditDto.setFilename3(filename3);
        } else {
            messageEditDto.setFilename3(messageService.findById(id).getFilename3());
        }

        messageService.updateById(id, messageEditDto);

        return "posts/edit";
    }

    @PostMapping(value = "/{id}/edit", params = "Delete3")
    public String deleteFile3(@ModelAttribute("message") @Valid MessageEditDto messageEditDto,
                              BindingResult bindingResult,
                              @PathVariable("id") Long id,
                              @RequestParam("files") List<MultipartFile> files) {
        if (bindingResult.hasErrors()) {
            return "posts/edit";
        }

        if (attachedFileDeleted = messageService.deleteAttachedFile3(id)) {
            messageEditDto.setFilename3(null);
            messageEditDto.setEdited(true);
            messageEditDto.setEditTime(LocalDateTime.now());
        }

        String filename1 = FileUploadUtil.getUuidFilename1(files);
        String filename2 = FileUploadUtil.getUuidFilename2(files);

        if (FileUploadUtil.upload1(uploadPath, filename1, files)) {
            messageEditDto.setFilename1(filename1);
        } else {
            messageEditDto.setFilename1(messageService.findById(id).getFilename1());
        }

        if (FileUploadUtil.upload2(uploadPath, filename2, files)) {
            messageEditDto.setFilename2(filename2);
        } else {
            messageEditDto.setFilename2(messageService.findById(id).getFilename2());
        }

        messageService.updateById(id, messageEditDto);

        return "posts/edit";
    }
}
