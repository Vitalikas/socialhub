package lt.kolinko.socialhub.controllers;

import lt.kolinko.socialhub.dto.MediaCreateDto;
import lt.kolinko.socialhub.dto.ProfileEditDto;
import lt.kolinko.socialhub.services.MediaService;
import lt.kolinko.socialhub.services.ProfileService;
import lt.kolinko.socialhub.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
@PreAuthorize("hasAuthority('USER')")
@RequestMapping("/users")
public class ProfileController {
    @Autowired
    private UserService userService;
    @Autowired
    private ProfileService profileService;
    @Autowired
    private MediaService mediaService;
    @Value("${upload.path}")
    private String uploadPath;
    @Value("${tomtom.apikey}")
    private String tomTomApiKey;

    @GetMapping("/{id}/profile/view")
    public String profileInfo(@PathVariable("id") Long id,
                              Model model) {
        model.addAttribute("user", userService.findById(id));

        return "profile/info";
    }

    @PreAuthorize("#id == authentication.principal.id")
    @GetMapping("/{id}/profile")
    public String showProfile(@PathVariable("id") Long id,
                              @ModelAttribute("profile") ProfileEditDto profileEditDto,
                              @ModelAttribute("media") MediaCreateDto mediaCreateDto,
//                              @RequestParam(value = "lng", required = false) Double longitude,
//                              @RequestParam(value = "lat", required = false) Double latitude,
                              Model model) {
        model.addAttribute("user", userService.findById(id));
//        model.addAttribute("apikey", tomTomApiKey);
//        model.addAttribute("gottenLocation", LocationAddingUtil.getGeolocation(longitude, latitude));
//        model.addAttribute("location", LocationAddingUtil.getLocation(longitude, latitude));

        return "profile/profile";
    }

    @PostMapping(value = "{id}/profile/update", params = "Update")
    public String update(@PathVariable("id") Long id,
                         Principal principal,
                         @ModelAttribute("profile") @Valid ProfileEditDto profileEditDto,
                         BindingResult bindingResult,
                         @ModelAttribute("media") MediaCreateDto mediaCreateDto,
                         @RequestParam("file") MultipartFile file,
                         @RequestParam("media") List<MultipartFile> files) {
        if (bindingResult.hasErrors()) {
            return "profile/profile";
        }

        profileService.update(id, profileEditDto, file);

        if (files.stream().anyMatch(f -> f.getSize() > 0)) {
            mediaService.save(mediaCreateDto, files, principal);
        }

        String userId = String.valueOf(id);
//        String lng = String.valueOf(longitude);
//        String lat = String.valueOf(latitude);

        return "redirect:/users/"+userId+"/profile";
    }

    @PostMapping(value = "{id}/profile/update", params = "Delete")
    public String deleteFile(@PathVariable("id") Long userId,
                             @RequestParam("checked") List<String> checkedFiles) {
        if (checkedFiles != null) {
            checkedFiles.forEach(chF -> {
                Long mediaId = Long.parseLong(chF);
                mediaService.delete(mediaId);
            });
        }

        String userId_ = String.valueOf(userId);
        return "redirect:/users/"+userId_+"/profile";
    }
}
