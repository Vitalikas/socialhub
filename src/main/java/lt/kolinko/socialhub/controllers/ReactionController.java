package lt.kolinko.socialhub.controllers;

import lt.kolinko.socialhub.dto.ReactionCreateDto;
import lt.kolinko.socialhub.entities.Reaction;
import lt.kolinko.socialhub.entities.ReactionType;
import lt.kolinko.socialhub.repositories.MessageRepository;
import lt.kolinko.socialhub.repositories.ReactionRepository;
import lt.kolinko.socialhub.repositories.UserRepository;
import lt.kolinko.socialhub.services.ReactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import java.security.Principal;

@Controller
public class ReactionController {
    @Autowired
    private ReactionService reactionService;
    @Autowired
    private ReactionRepository reactionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MessageRepository messageRepository;

    @PostMapping(value = "/post/{id}/react", params = "like")
    public String likePost(@ModelAttribute("reaction") ReactionCreateDto dto,
                           @PathVariable("id") Long postId,
                           Principal principal) {
        Reaction reaction = reactionService.get(postId, principal);
        if (reaction != null) {
            if (reaction.getReactionType() == ReactionType.LIKE) {
                reactionService.remove(reaction);
            } else {
                dto.setReactionType(ReactionType.LIKE);
                reactionService.update(reaction, dto);
            }
        } else {
            dto.setReactionAuthor(userRepository.findByEmail(principal.getName()));
            dto.setReactionType(ReactionType.LIKE);
            reactionService.save(postId, dto);
        }
        return "redirect:/";
    }

    @PostMapping(value = "/post/{id}/react", params = "dislike")
    public String dislikePost(@ModelAttribute("reaction") ReactionCreateDto dto,
                              @PathVariable("id") Long postId,
                              Principal principal) {
        Reaction reaction = reactionService.get(postId, principal);
        if (reaction != null) {
            if (reaction.getReactionType() == ReactionType.DISLIKE) {
                reactionService.remove(reaction);
            } else {
                dto.setReactionType(ReactionType.DISLIKE);
                reactionService.update(reaction, dto);
            }
        } else {
            dto.setReactionAuthor(userRepository.findByEmail(principal.getName()));
            dto.setReactionType(ReactionType.DISLIKE);
            reactionService.save(postId, dto);
        }
        return "redirect:/";
    }
}
