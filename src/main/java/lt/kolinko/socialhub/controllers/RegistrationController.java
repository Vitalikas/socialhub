package lt.kolinko.socialhub.controllers;

import lt.kolinko.socialhub.dto.UserDto;
import lt.kolinko.socialhub.dto.UserRegistrationDto;
import lt.kolinko.socialhub.entities.User;
import lt.kolinko.socialhub.repositories.UserRepository;
import lt.kolinko.socialhub.services.MailService;
import lt.kolinko.socialhub.services.UserService;
import lt.kolinko.socialhub.utils.FileUploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/registration")
public class RegistrationController {
    @Autowired
    private UserService userService;
    @Autowired
    private MailService mailService;
    @Autowired
    private UserRepository userRepository;
    @Value("${upload.path}")
    private String uploadPath;
    @Value("${server.path}")
    private String serverPath;

    @GetMapping
    public String showRegistrationForm(@ModelAttribute("user") UserRegistrationDto userRegistrationDto) {
        return "registration/registration";
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("user") @Valid UserRegistrationDto userRegistrationDto,
                                      BindingResult result,
                                      @RequestParam("file") MultipartFile file,
                                      Model model) {
        if (result.hasErrors()) {
            return "registration/registration";
        }

        UserDto userFromDb = userService.findByEmail(userRegistrationDto.getEmail());
        if (userFromDb != null) {
            model.addAttribute("message", "User exists!");
            return "registration/registration";
        }

        FileUploadUtil.createUploadDir(uploadPath);
        String filename = FileUploadUtil.getUuidFilename(file);
        if (FileUploadUtil.upload(uploadPath, filename, file)) {
            userRegistrationDto.setPicture(filename);
        }

        userService.save(userRegistrationDto);

        User user = Optional.of(userRepository
                .findByEmail(userRegistrationDto.getEmail()))
                .orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find record with email: %s",
                        userRegistrationDto.getEmail())));

        mailService.send(
                user.getEmail(),
                "Activation code",
                String.format("Hello, %s! Welcome to socialHub. " +
                        "Please follow the link to complete registration: %s/registration/activate/%s",
                        user.getUsername(),
                        serverPath,
                        user.getVerificationToken()));

        return "redirect:/login";
    }

    @GetMapping("/activate/{token}")
    public String activate(@PathVariable String token,
                           Model model) {
        boolean isActivated = userService.activateUser(token);

        if (isActivated) {
            model.addAttribute("message", "Activation was successful and profile is now activated. Please login.");
        } else {
            model.addAttribute("message", "Activation error. Verification token was used or expired.");
        }

        return "login/login";
    }
}
