package lt.kolinko.socialhub.controllers;

import lt.kolinko.socialhub.dto.EmailChangeDto;
import lt.kolinko.socialhub.dto.PassChangeDto;
import lt.kolinko.socialhub.services.ProfileService;
import lt.kolinko.socialhub.services.SecurityService;
import lt.kolinko.socialhub.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@Controller
@PreAuthorize("hasAuthority('USER')")
@RequestMapping("/users")
public class SecurityController {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserService userService;
    @Autowired
    private ProfileService profileService;
    @Autowired
    private SecurityService securityService;

    @PreAuthorize("#id == authentication.principal.id")
    @GetMapping("{id}/profile/security/email/update")
    public String showEmailChangeForm(@PathVariable("id") Long id,
                                  @ModelAttribute("email") EmailChangeDto dto,
                                  Model model) {
        model.addAttribute("user", userService.findById(id));

        return "profile/security-email";
    }

    @PreAuthorize("#id == authentication.principal.id")
    @GetMapping("{id}/profile/security/pass/update")
    public String showPassChangeForm(@PathVariable("id") Long id,
                                     @ModelAttribute("pass") PassChangeDto dto,
                                     Model model) {
        model.addAttribute("user", userService.findById(id));
        return "profile/security-pass";
    }

    @PostMapping("{id}/profile/security/email/update")
    public String changeEmail(@PathVariable("id") Long id,
                              @ModelAttribute("email") @Valid EmailChangeDto dto,
                              BindingResult bindingResult,
                              Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("user", userService.findById(id));
            return "profile/security-email";
        }

        if (passwordEncoder.matches(dto.getPassword(), userService.findById(id).getPassword())) {
            securityService.updateEmail(id, dto);
        }

        return "redirect:/logout";
    }

    @PostMapping("{id}/profile/security/pass/update")
    public String changePass(@PathVariable("id") Long id,
                              @ModelAttribute("pass") @Valid PassChangeDto dto,
                              BindingResult bindingResult,
                              Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("user", userService.findById(id));
            return "profile/security-pass";
        }

        if (passwordEncoder.matches(dto.getPassword(), userService.findById(id).getPassword())) {
            securityService.updatePass(id, dto);
        }

        return "redirect:/logout";
    }
}
