package lt.kolinko.socialhub.controllers;

import lt.kolinko.socialhub.dto.UserEditDto;
import lt.kolinko.socialhub.entities.Role;
import lt.kolinko.socialhub.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@Controller
@PreAuthorize("hasAuthority('USER')")
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping
    public String showAllUsers(Model model) {
        model.addAttribute("users", userService.findAll());
        return "users/users";
    }

    @GetMapping("/{id}")
    public String showUser(@PathVariable("id") Long id,
                           Model model) {
        model.addAttribute("user", userService.findById(id));
        return "users/user";
    }

    /*
    another variant using @PathVariable User user (userRepo or userService not needed)

    @GetMapping("/{user}")
    public String showUser(@PathVariable User user, Model model) {
        model.addAttribute("user", user);
        return "users/user";
    }
     */

    @GetMapping("/{id}/edit")
    public String showUserEditForm(@PathVariable("id") Long id,
                                   Model model) {
        model.addAttribute("user", userService.findById(id));
        model.addAttribute("roles", Role.values());
        return "users/edit";
    }

    @PostMapping("/{id}/edit")
    public String updateUser(@ModelAttribute("user") @Valid UserEditDto userEditDto,
                             BindingResult bindingResult,
                             @PathVariable("id") Long id,
                             Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("roles", Role.values());
            return "users/edit";
        }
        userService.update(id, userEditDto);
        return "redirect:/users";
    }

    @PostMapping(value = "/{id}/delete", params = "delete")
    public String deleteUser(@PathVariable("id") Long id) {
        userService.delete(id);
        return "redirect:/users";
    }

    @PostMapping(value = "/{id}/delete", params = "ban")
    public String banUser(@PathVariable("id") Long id) {
        userService.ban(id);
        return "redirect:/users";
    }

    @GetMapping("/{id}/message")
    public String showPrivateMessageForm(@PathVariable("id") Long id) {
        return "users/private";
    }
}
