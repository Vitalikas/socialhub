package lt.kolinko.socialhub.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Lob;
import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CommentCreateDto {
    @NotEmpty(message = "content should not be empty")
    @Lob
    private String content;

    private String filename1;
}
