package lt.kolinko.socialhub.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lt.kolinko.socialhub.entities.Message;
import lt.kolinko.socialhub.entities.User;

import javax.persistence.Lob;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CommentDto {
    private Long id;
    private String content;
    private String filename1;
    private LocalDateTime time;
    private Message message;
    private User commentAuthor;
}
