package lt.kolinko.socialhub.dto;

import lombok.Getter;
import lombok.Setter;
import lt.kolinko.socialhub.constraints.FieldMatch;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@FieldMatch.List({
        @FieldMatch(first = "email", second = "emailConfirmation", message = "The email fields must match")
})
@Getter
@Setter
public class EmailChangeDto {
    @Email
    @NotEmpty(message = "email should not be empty")
    private String email;
    @Email
    @NotEmpty(message = "email should not be empty")
    private String emailConfirmation;
    @NotEmpty(message = "password should not be empty")
    private String password;
}
