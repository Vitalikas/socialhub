package lt.kolinko.socialhub.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Lob;
import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MessageCreateDto {
    @NotEmpty(message = "content should not be empty")
    @Lob
    private String text;

    @NotEmpty(message = "tag should not be empty")
    private String tag;

    private String filename1;
    private String filename2;
    private String filename3;
    private Double lat;
    private Double lng;
}
