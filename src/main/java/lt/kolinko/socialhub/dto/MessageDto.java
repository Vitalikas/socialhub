package lt.kolinko.socialhub.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lt.kolinko.socialhub.entities.Comment;
import lt.kolinko.socialhub.entities.Reaction;
import lt.kolinko.socialhub.entities.User;
import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MessageDto {
    private Long id;
    private String text;
    private String tag;
    private String filename1;
    private String filename2;
    private String filename3;
    private User author;
    private LocalDateTime time;
    private boolean edited;
    private LocalDateTime editTime;
    private List<Comment> comments;
    private double[] latlng;
    private List<Reaction> reactions;
}
