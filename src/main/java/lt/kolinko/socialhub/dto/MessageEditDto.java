package lt.kolinko.socialhub.dto;

import lombok.Getter;
import lombok.Setter;
import lt.kolinko.socialhub.entities.Comment;
import javax.persistence.Lob;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
public class MessageEditDto {
    private Long id;

    @NotEmpty(message = "content should not be empty")
    @Lob
    private String text;

    @NotEmpty(message = "tag should not be empty")
    private String tag;

    private String filename1;
    private String filename2;
    private String filename3;
    private boolean edited;
    private LocalDateTime editTime;
    private Set<Comment> comments;
}
