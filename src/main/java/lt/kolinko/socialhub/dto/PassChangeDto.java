package lt.kolinko.socialhub.dto;

import lombok.Getter;
import lombok.Setter;
import lt.kolinko.socialhub.constraints.FieldMatch;
import javax.validation.constraints.Size;

@FieldMatch.List({
        @FieldMatch(first = "newPassword", second = "newPasswordConfirmation", message = "The password fields must match")
})
@Getter
@Setter
public class PassChangeDto {
    private String password;
    @Size(min = 7, max = 50, message = "password must be at least 7 characters long")
    private String newPassword;
    @Size(min = 7, max = 50, message = "password must be at least 7 characters long")
    private String newPasswordConfirmation;
}
