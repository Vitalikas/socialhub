package lt.kolinko.socialhub.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
public class ProfileEditDto {
    @NotEmpty(message = "username should not be empty")
    @Size(min = 3, max = 50, message = "username must be at least 3 characters long")
    private String username;
    private String picture;
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dof;
    private String homeTown;
    private String livesIn;
}
