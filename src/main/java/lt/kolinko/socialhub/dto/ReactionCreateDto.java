package lt.kolinko.socialhub.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lt.kolinko.socialhub.entities.ReactionType;
import lt.kolinko.socialhub.entities.User;

@NoArgsConstructor
@Getter
@Setter
public class ReactionCreateDto {
    private Long id;
    private ReactionType reactionType;
    private User reactionAuthor;
}
