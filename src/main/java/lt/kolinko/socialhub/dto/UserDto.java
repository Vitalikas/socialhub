package lt.kolinko.socialhub.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lt.kolinko.socialhub.entities.Media;
import lt.kolinko.socialhub.entities.Role;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
public class UserDto {
    private Long id;
    private String username;
    private String email;
    private String emailConfirmation;
    private String password;
    private String newPassword;
    private String newPasswordConfirmation;
    private String picture;
    private boolean active;
    private Set<Role> roles;
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dof;
    private String homeTown;
    private String livesIn;
    private List<Media> mediaList;

    public UserDto(Long id, String username, String email, String password, String picture,
                   boolean active, Set<Role> roles, Date dof, String homeTown, String livesIn, List<Media> mediaList) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.picture = picture;
        this.active = active;
        this.roles = roles;
        this.dof = dof;
        this.homeTown = homeTown;
        this.livesIn = livesIn;
        this.mediaList = mediaList;
    }
}
