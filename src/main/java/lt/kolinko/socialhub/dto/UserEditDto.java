package lt.kolinko.socialhub.dto;

import lombok.Getter;
import lombok.Setter;
import lt.kolinko.socialhub.entities.Role;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
public class UserEditDto {
    private Long id;

    @NotEmpty(message = "username should not be empty")
    @Size(min = 3, max = 50, message = "username must be at least 3 characters long")
    private String username;

    @NotEmpty(message = "at least one role must be selected")
    private Set<Role> roles;
}
