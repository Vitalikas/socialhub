package lt.kolinko.socialhub.dto;

import lombok.Getter;
import lombok.Setter;
import lt.kolinko.socialhub.constraints.FieldMatch;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@FieldMatch.List({
        @FieldMatch(first = "password", second = "passwordConfirmation", message = "The password fields must match")
})
@Getter
@Setter
public class UserRegistrationDto {
    @NotEmpty(message = "username should not be empty")
    @Size(min = 3, max = 50, message = "username must be at least 3 characters long")
    private String username;

    @Email
    @NotEmpty(message = "email should not be empty")
    private String email;

    @NotEmpty(message = "password should not be empty")
    @Size(min = 7, max = 50, message = "password must be at least 7 characters long")
    private String password;

    @NotEmpty(message = "password should not be empty")
    @Size(min = 7, max = 50, message = "password must be at least 7 characters long")
    private String passwordConfirmation;

    private String picture;

    @NotEmpty(message = "location should not be empty")
    private String livesIn;
}
