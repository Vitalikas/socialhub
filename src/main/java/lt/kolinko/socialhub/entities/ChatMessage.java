package lt.kolinko.socialhub.entities;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ChatMessage {
    private final MessageType type;
    private final String content;
    private final String sender;
    private final String time;
}
