package lt.kolinko.socialhub.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.standard.StandardFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "messages")
@Indexed // Lucene indexing, for Hibernate Search
/*
The Stop Words ["a", "an", "and", "are", "as", "at", "be", "but", "by", "for", "if", "in", "into", "is", "it", "no",
"not", "of", "on", "or", "such", "that", "the", "their", "then", "there", "these", "they", "this", "to", "was", "will",
and "with"] and their existence in terms, databases, or files that are to be indexed/searched by Lucene
can lead to any of the following:

1. Stop Words being Ignored/Filtered during the Lucene Indexing Process

2. Stop Words being Ignored/Filtered during the Lucene Querying Process

3. No Result for Queries that Include, Start With or End With any Stop Word

The way to solve this problem or to handle them during both indexing and searching process is
to define Your Custom Analyzer
 */
@AnalyzerDef(
        name = "customTextAnalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = StandardFilterFactory.class)
        })
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Field(analyzer = @Analyzer(definition = "customTextAnalyzer")) // field pointer for Hibernate Search
    @Lob
    private String text;

    @Field(analyzer = @Analyzer(definition = "customTextAnalyzer"))
    private String tag;

    private String filename1;
    private String filename2;
    private String filename3;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "user_id")
    @IndexedEmbedded // Hibernate Search
    private User author;

    private LocalDateTime time;
    private boolean edited;
    private LocalDateTime editTime;

    @OneToMany(mappedBy = "message", cascade = CascadeType.ALL)
    private List<Comment> comments;

    @OneToMany(mappedBy = "message", cascade = CascadeType.ALL)
    private List<MessageAttachment> messageAttachments;

    private double[] latlng;

    @OneToMany(mappedBy = "message", cascade = CascadeType.ALL)
    private List<Reaction> reactions;
}
