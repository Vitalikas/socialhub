package lt.kolinko.socialhub.entities;

public enum MessageType {
    CHAT, CONNECT, DISCONNECT
}
