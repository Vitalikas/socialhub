package lt.kolinko.socialhub.entities;

public enum ReactionType {
    LIKE, DISLIKE
}
