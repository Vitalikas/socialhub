package lt.kolinko.socialhub.entities;

public enum Role {
    USER, ADMIN
}
