package lt.kolinko.socialhub.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Field(analyzer = @Analyzer(definition = "customTextAnalyzer")) // Hibernate_search, embedded into Message.class field User author
    private String username;

    private String email;
    private String password;
    private String picture;
    private boolean active;

    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
    @Enumerated(EnumType.STRING)
    private Set<Role> roles;

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
    private List<Message> messages;

    @OneToMany(mappedBy = "commentAuthor", cascade = CascadeType.ALL)
    private List<Comment> comments;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dof;
    private String homeTown;
    private String livesIn;
    private String ip; // unused

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Location> locations; // unused

    private String verificationToken;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Media> mediaList;

    public User(String username, String email, String password, String picture, boolean active, Set<Role> roles) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.picture = picture;
        this.active = active;
        this.roles = roles;
    }
}
