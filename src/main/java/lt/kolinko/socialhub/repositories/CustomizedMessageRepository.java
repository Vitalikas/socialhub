package lt.kolinko.socialhub.repositories;

import lt.kolinko.socialhub.entities.Message;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface CustomizedMessageRepository {
    List<Message> search(String text, int limit, int offset);
}
