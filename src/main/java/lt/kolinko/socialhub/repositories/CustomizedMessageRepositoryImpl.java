package lt.kolinko.socialhub.repositories;

import lt.kolinko.socialhub.entities.Message;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class CustomizedMessageRepositoryImpl implements CustomizedMessageRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Message> search(String searchCriteria, int limit, int offset) {
        // get the full text entity manager
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(em);

        // create the Hibernate_Search query builder
        QueryBuilder queryBuilder = fullTextEntityManager.getSearchFactory()
                .buildQueryBuilder().forEntity(Message.class).get();

        // create the query by keywords using Hibernate_Search query builder
        org.apache.lucene.search.Query luceneQuery = queryBuilder
                .keyword()
                    .onFields("tag", "text", "author.username")
                .matching(searchCriteria)
                .createQuery();

        // wrap Lucene query in a javax.persistence.Query
        javax.persistence.Query jpaQuery = fullTextEntityManager.createFullTextQuery(luceneQuery, Message.class);

        jpaQuery.setMaxResults(limit);
        jpaQuery.setFirstResult(offset);

        @SuppressWarnings("unchecked")
        // execute search
        List<Message> results = jpaQuery.getResultList();

        return results;
    }
}
