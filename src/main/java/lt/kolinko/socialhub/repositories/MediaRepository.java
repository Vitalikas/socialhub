package lt.kolinko.socialhub.repositories;

import lt.kolinko.socialhub.entities.Media;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MediaRepository extends JpaRepository<Media, Long> {
}
