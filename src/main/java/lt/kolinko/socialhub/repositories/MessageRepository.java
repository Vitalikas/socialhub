package lt.kolinko.socialhub.repositories;

import lt.kolinko.socialhub.entities.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long>, CustomizedMessageRepository {
}
