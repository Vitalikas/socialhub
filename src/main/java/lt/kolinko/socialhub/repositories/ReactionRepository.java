package lt.kolinko.socialhub.repositories;

import lt.kolinko.socialhub.entities.Message;
import lt.kolinko.socialhub.entities.Reaction;
import lt.kolinko.socialhub.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReactionRepository extends JpaRepository<Reaction, Long> {
    Reaction findByReactionAuthor(User user);
    Reaction findByMessageAndReactionAuthor(Message message, User user);
}
