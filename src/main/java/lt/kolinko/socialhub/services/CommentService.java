package lt.kolinko.socialhub.services;

import lt.kolinko.socialhub.dto.CommentCreateDto;
import lt.kolinko.socialhub.dto.CommentDto;
import lt.kolinko.socialhub.entities.Comment;
import lt.kolinko.socialhub.repositories.CommentRepository;
import lt.kolinko.socialhub.repositories.MessageRepository;
import lt.kolinko.socialhub.repositories.UserRepository;
import lt.kolinko.socialhub.utils.FileUploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentService implements ICommentService {
    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private MessageService messageService;
    @Value("${upload.path}")
    private String uploadPath;

    @Override
    public List<CommentDto> showAll() {
        return commentRepository.findAll().stream()
                .map(c -> new CommentDto(
                        c.getId(),
                        c.getContent(),
                        c.getFilename1(),
                        c.getTime(),
                        c.getMessage(),
                        c.getCommentAuthor()
                )).collect(Collectors.toList());
    }

    @Override
    public void save(Long postId, CommentCreateDto commentCreateDto, MultipartFile file, Principal principal) {
        // uploading and attaching picture to comment
        FileUploadUtil.createUploadDir(uploadPath);
        String filename1 = FileUploadUtil.getUuidFilename(file);
        if (FileUploadUtil.upload(uploadPath, filename1, file)) {
            commentCreateDto.setFilename1(filename1);
        }

        Comment comment = new Comment();
        comment.setContent(commentCreateDto.getContent());
        comment.setFilename1(commentCreateDto.getFilename1());
        comment.setTime(LocalDateTime.now());
        comment.setMessage(messageRepository.findById(postId).get());
        comment.setCommentAuthor(userRepository.findByEmail(principal.getName()));

        commentRepository.save(comment);
    }
}
