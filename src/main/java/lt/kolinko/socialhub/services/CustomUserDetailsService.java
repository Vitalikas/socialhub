package lt.kolinko.socialhub.services;

import lt.kolinko.socialhub.entities.UserPrincipal;
import lt.kolinko.socialhub.entities.Role;
import lt.kolinko.socialhub.entities.User;
import lt.kolinko.socialhub.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) {
        User user = Optional.of(userRepository.findByEmail(email))
                .orElseThrow(() -> new UsernameNotFoundException(String.format("User with email '%s' not found", email)));

        Collection<GrantedAuthority> authorities = mapRolesToAuthorities(user.getRoles());

        return buildUserForAuthentication(user, authorities);
    }

    org.springframework.security.core.userdetails.User buildUserForAuthentication(User user, Collection<GrantedAuthority> authorities) {
        String email = user.getEmail();
        String password = user.getPassword();
        boolean enabled = user.isActive();
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        Long id = user.getId();

        return new UserPrincipal(
                email,
                password,
                enabled,
                accountNonExpired,
                credentialsNonExpired,
                accountNonLocked,
                authorities,
                id);
    }

    private Collection<GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.name()))
                .collect(Collectors.toList());
    }


}
