package lt.kolinko.socialhub.services;

import lt.kolinko.socialhub.dto.CommentCreateDto;
import lt.kolinko.socialhub.dto.CommentDto;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;
import java.util.List;

public interface ICommentService {
    List<CommentDto> showAll();
    void save(Long postId, CommentCreateDto commentCreateDto, MultipartFile file, Principal principal);
}
