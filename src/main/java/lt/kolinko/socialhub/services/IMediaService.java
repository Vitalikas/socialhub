package lt.kolinko.socialhub.services;

import lt.kolinko.socialhub.dto.MediaCreateDto;
import org.springframework.web.multipart.MultipartFile;
import java.security.Principal;
import java.util.List;

public interface IMediaService {
    void save(MediaCreateDto mediaCreateDto, List<MultipartFile> files, Principal principal);
    void delete(Long id);
}
