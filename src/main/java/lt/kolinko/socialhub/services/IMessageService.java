package lt.kolinko.socialhub.services;

import lt.kolinko.socialhub.dto.MessageCreateDto;
import lt.kolinko.socialhub.dto.MessageDto;
import lt.kolinko.socialhub.dto.MessageEditDto;
import org.springframework.web.multipart.MultipartFile;
import java.security.Principal;
import java.util.List;

public interface IMessageService {
    MessageDto findById(Long id);
    List<MessageDto> searchByText(String text, int limit, int offset);
    void save(MessageCreateDto messageCreateDto, List<MultipartFile> files, Principal principal);
    void updateById(Long id, MessageEditDto messageEditDto);
    void deleteById(Long id);
    boolean deleteAttachedFile1(Long messageId);
    boolean deleteAttachedFile2(Long messageId);
    boolean deleteAttachedFile3(Long messageId);
}
