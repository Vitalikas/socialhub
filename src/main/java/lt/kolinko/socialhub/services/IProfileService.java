package lt.kolinko.socialhub.services;

import lt.kolinko.socialhub.dto.ProfileEditDto;
import org.springframework.web.multipart.MultipartFile;

public interface IProfileService {
    void update(Long id, ProfileEditDto profileEditDto, MultipartFile file);
}
