package lt.kolinko.socialhub.services;

import lt.kolinko.socialhub.dto.ReactionCreateDto;
import lt.kolinko.socialhub.entities.Reaction;

import java.security.Principal;

public interface IReactionService {
    Reaction get(Long postId, Principal principal);
    void update(Reaction reaction, ReactionCreateDto reactionCreateDto);
    void save(Long postId, ReactionCreateDto reactionCreateDto);
    void remove(Reaction reaction);
}
