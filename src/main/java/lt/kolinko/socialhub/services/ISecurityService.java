package lt.kolinko.socialhub.services;

import lt.kolinko.socialhub.dto.EmailChangeDto;
import lt.kolinko.socialhub.dto.PassChangeDto;

public interface ISecurityService {
    void updateEmail(Long id, EmailChangeDto dto);
    void updatePass(Long id, PassChangeDto dto);
}
