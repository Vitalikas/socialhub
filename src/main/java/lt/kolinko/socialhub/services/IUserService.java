package lt.kolinko.socialhub.services;

import lt.kolinko.socialhub.dto.UserDto;
import lt.kolinko.socialhub.dto.UserEditDto;
import lt.kolinko.socialhub.dto.UserRegistrationDto;
import java.util.List;

public interface IUserService {
    List<UserDto> findAll();
    UserDto findById(Long id);
    UserDto findByEmail(String email);
    void update(Long id, UserEditDto userEditDto);
    void delete(Long id);
    void save(UserRegistrationDto userRegistrationDto);
    void ban(Long id);
}
