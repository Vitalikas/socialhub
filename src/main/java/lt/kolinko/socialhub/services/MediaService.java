package lt.kolinko.socialhub.services;

import lt.kolinko.socialhub.dto.MediaCreateDto;
import lt.kolinko.socialhub.entities.Media;
import lt.kolinko.socialhub.repositories.MediaRepository;
import lt.kolinko.socialhub.repositories.UserRepository;
import lt.kolinko.socialhub.utils.FileDeleteUtil;
import lt.kolinko.socialhub.utils.FileUploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.persistence.EntityNotFoundException;
import java.security.Principal;
import java.util.Collections;
import java.util.List;

@Service
public class MediaService implements IMediaService {
    @Autowired
    private MediaRepository mediaRepository;
    @Autowired
    private UserRepository userRepository;
    @Value("${upload.path}")
    private String uploadPath;

    @Override
    public void save(MediaCreateDto mediaCreateDto, List<MultipartFile> files, Principal principal) {
        FileUploadUtil.createUploadDir(uploadPath);

        List<String> filenames = FileUploadUtil.getUuidFilenames(files);

        if (FileUploadUtil.uploadAll(uploadPath, filenames, files)) {
            filenames.forEach(filename -> {
                mediaCreateDto.setFilename(filename);

                Media media = new Media();
                media.setFilename(mediaCreateDto.getFilename());
                media.setUser(userRepository.findByEmail(principal.getName()));
                mediaRepository.save(media);
            });
        }
    }

    @Override
    public void delete(Long id) {
        Media m = mediaRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException(String.format("Cannot find record with ID: %d", id)));
        if (m != null) {
            FileDeleteUtil.deleteAll(Collections.singletonList(m.getFilename()), uploadPath);
            mediaRepository.delete(m);
        }
    }
}
