package lt.kolinko.socialhub.services;

import lt.kolinko.socialhub.dto.MessageCreateDto;
import lt.kolinko.socialhub.dto.MessageDto;
import lt.kolinko.socialhub.dto.MessageEditDto;
import lt.kolinko.socialhub.entities.Message;
import lt.kolinko.socialhub.repositories.MessageRepository;
import lt.kolinko.socialhub.repositories.CustomizedMessageRepositoryImpl;
import lt.kolinko.socialhub.repositories.UserRepository;
import lt.kolinko.socialhub.utils.FileDeleteUtil;
import lt.kolinko.socialhub.utils.FileUploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.persistence.EntityNotFoundException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MessageService implements IMessageService {
    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CustomizedMessageRepositoryImpl customizedMessageRepository;
    @Autowired
    private UserService userService;
    @Value("${upload.path}")
    private String uploadPath;

    @Override
    public MessageDto findById(Long id) {
        Message m = messageRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find message with ID: %d", id)));
        return new MessageDto(
                m.getId(),
                m.getText(),
                m.getTag(),
                m.getFilename1(),
                m.getFilename2(),
                m.getFilename3(),
                m.getAuthor(),
                m.getTime(),
                m.isEdited(),
                m.getEditTime(),
                m.getComments(),
                m.getLatlng(),
                m.getReactions()
        );
    }

    @Override
    public void save(MessageCreateDto messageCreateDto, List<MultipartFile> files, Principal principal) {
        FileUploadUtil.createUploadDir(uploadPath);

        String filename1 = FileUploadUtil.getUuidFilename1(files);
        String filename2 = FileUploadUtil.getUuidFilename2(files);
        String filename3 = FileUploadUtil.getUuidFilename3(files);

        if (FileUploadUtil.upload1(uploadPath, filename1, files)) {
            messageCreateDto.setFilename1(filename1);
        }

        if (FileUploadUtil.upload2(uploadPath, filename2, files)) {
            messageCreateDto.setFilename2(filename2);
        }

        if (FileUploadUtil.upload3(uploadPath, filename3, files)) {
            messageCreateDto.setFilename3(filename3);
        }

        double[] latlng = null;
        if (messageCreateDto.getLat() != null && messageCreateDto.getLng() != null) {
            latlng = new double[]{messageCreateDto.getLat(), messageCreateDto.getLng()};
        }

        Message message = new Message();
        message.setText(messageCreateDto.getText());
        message.setTag(messageCreateDto.getTag());
        message.setFilename1(messageCreateDto.getFilename1());
        message.setFilename2(messageCreateDto.getFilename2());
        message.setFilename3(messageCreateDto.getFilename3());
        message.setAuthor(userRepository.findByEmail(principal.getName()));
        message.setTime(LocalDateTime.now());
        message.setLatlng(latlng);

        messageRepository.save(message);
    }

    @Override
    public void updateById(Long id, MessageEditDto messageEditDto) {
        Message messageToUpdate = messageRepository
                .findById(messageEditDto.getId())
                .orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find record with ID: %d", id)));
        messageToUpdate.setText(messageEditDto.getText());
        messageToUpdate.setTag(messageEditDto.getTag());
        messageToUpdate.setFilename1(messageEditDto.getFilename1());
        messageToUpdate.setFilename2(messageEditDto.getFilename2());
        messageToUpdate.setFilename3(messageEditDto.getFilename3());
        messageToUpdate.setEdited(messageEditDto.isEdited());
        messageToUpdate.setEditTime(messageEditDto.getEditTime());
        messageRepository.save(messageToUpdate);
    }

    @Override
    public void deleteById(Long id) {
        Message message = messageRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException(String.format("Cannot find record with ID: %d", id)));
        FileDeleteUtil.deleteAll(Arrays.asList(
                message.getFilename1(),
                message.getFilename2(),
                message.getFilename3()), uploadPath);
        messageRepository.deleteById(id);
    }

    @Override
    public boolean deleteAttachedFile1(Long id) {
        boolean deleted = false;
        Message message = messageRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException(String.format("Cannot find record with ID: %d", id)));
        if (message != null) {
            FileDeleteUtil.deleteAll(Collections.singletonList(message.getFilename1()), uploadPath);
            deleted = true;
        }
        return deleted;
    }

    @Override
    public boolean deleteAttachedFile2(Long id) {
        boolean deleted = false;
        Message message = messageRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException(String.format("Cannot find record with ID: %d", id)));
        if (message != null) {
            FileDeleteUtil.deleteAll(Collections.singletonList(message.getFilename2()), uploadPath);
            deleted = true;
        }
        return deleted;
    }

    @Override
    public boolean deleteAttachedFile3(Long id) {
        boolean deleted = false;
        Message message = messageRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException(String.format("Cannot find record with ID: %d", id)));
        if (message != null) {
            FileDeleteUtil.deleteAll(Collections.singletonList(message.getFilename3()), uploadPath);
            deleted = true;
        }
        return deleted;
    }

    @Override
    public List<MessageDto> searchByText(String text, int limit, int offset) {
        return text != null && !text.isEmpty()
                ? customizedMessageRepository.search(text, limit, offset).stream()
                .map(m -> new MessageDto(
                        m.getId(),
                        m.getText(),
                        m.getTag(),
                        m.getFilename1(),
                        m.getFilename2(),
                        m.getFilename3(),
                        m.getAuthor(),
                        m.getTime(),
                        m.isEdited(),
                        m.getEditTime(),
                        m.getComments(),
                        m.getLatlng(),
                        m.getReactions())).collect(Collectors.toList())
                : messageRepository.findAll().stream()
                .map(m -> new MessageDto(
                        m.getId(),
                        m.getText(),
                        m.getTag(),
                        m.getFilename1(),
                        m.getFilename2(),
                        m.getFilename3(),
                        m.getAuthor(),
                        m.getTime(),
                        m.isEdited(),
                        m.getEditTime(),
                        m.getComments(),
                        m.getLatlng(),
                        m.getReactions())).collect(Collectors.toList());
    }
}
