package lt.kolinko.socialhub.services;

import lt.kolinko.socialhub.dto.ProfileEditDto;
import lt.kolinko.socialhub.entities.User;
import lt.kolinko.socialhub.repositories.UserRepository;
import lt.kolinko.socialhub.utils.FileUploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.persistence.EntityNotFoundException;

@Service
public class ProfileService implements IProfileService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
    @Value("${upload.path}")
    private String uploadPath;

    @Override
    public void update(Long id, ProfileEditDto profileEditDto, MultipartFile file) {
        String filename = FileUploadUtil.getUuidFilename(file);

        if (FileUploadUtil.upload(uploadPath, filename, file)) {
            profileEditDto.setPicture(filename);
        } else {
            profileEditDto.setPicture(userService.findById(id).getPicture());
        }

        User userToUpdate = userRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find record with ID: %d", id)));

        userToUpdate.setUsername(profileEditDto.getUsername());
        userToUpdate.setDof(profileEditDto.getDof());
        userToUpdate.setHomeTown(profileEditDto.getHomeTown());
        userToUpdate.setLivesIn(profileEditDto.getLivesIn());
        userToUpdate.setPicture(profileEditDto.getPicture());

        userRepository.save(userToUpdate);
    }
}
