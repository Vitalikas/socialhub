package lt.kolinko.socialhub.services;

import lt.kolinko.socialhub.dto.ReactionCreateDto;
import lt.kolinko.socialhub.entities.Message;
import lt.kolinko.socialhub.entities.Reaction;
import lt.kolinko.socialhub.entities.User;
import lt.kolinko.socialhub.repositories.MessageRepository;
import lt.kolinko.socialhub.repositories.ReactionRepository;
import lt.kolinko.socialhub.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.security.Principal;

@Service
public class ReactionService implements IReactionService {
    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ReactionRepository reactionRepository;

    @Override
    public Reaction get(Long postId, Principal principal) {
        Message message = messageRepository.findById(postId).get();
        User author = userRepository.findByEmail(principal.getName());
        return reactionRepository.findByMessageAndReactionAuthor(message, author);
    }

    @Override
    public void update(Reaction reaction, ReactionCreateDto dto) {
        reaction.setReactionType(dto.getReactionType());
        reactionRepository.save(reaction);
    }

    @Override
    public void remove(Reaction reaction) {
        reactionRepository.delete(reaction);
    }

    @Override
    public void save(Long postId, ReactionCreateDto dto) {
        Reaction reaction = new Reaction();
        reaction.setMessage(messageRepository.findById(postId).get());
        reaction.setReactionAuthor(dto.getReactionAuthor());
        reaction.setReactionType(dto.getReactionType());
        reactionRepository.save(reaction);
    }
}
