package lt.kolinko.socialhub.services;

import lt.kolinko.socialhub.dto.EmailChangeDto;
import lt.kolinko.socialhub.dto.PassChangeDto;
import lt.kolinko.socialhub.entities.User;
import lt.kolinko.socialhub.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@Service
public class SecurityService implements ISecurityService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MailService mailService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Value("${server.path}")
    private String serverPath;

    String verificationToken = UUID.randomUUID().toString();

    @Override
    public void updateEmail(Long id, EmailChangeDto dto) {
        User userToUpdate = userRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find record with ID: %d", id)));

        userToUpdate.setEmail(dto.getEmail());
        userToUpdate.setActive(false);
        userToUpdate.setVerificationToken(verificationToken);

        mailService.send(
                dto.getEmail(),
                "Activation code",
                String.format("Hello, %s! Welcome to socialHub. " +
                                "Request for changing email address was successful. " +
                                "Please follow the link to activate account: %s/registration/activate/%s",
                        userToUpdate.getUsername(),
                        serverPath,
                        verificationToken)
        );

        userRepository.save(userToUpdate);
    }

    @Override
    public void updatePass(Long id, PassChangeDto dto) {
        User userToUpdate = userRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find record with ID: %d", id)));

        userToUpdate.setPassword(passwordEncoder.encode(dto.getNewPassword()));
        userToUpdate.setActive(false);
        userToUpdate.setVerificationToken(verificationToken);

        mailService.send(
                userToUpdate.getEmail(),
                "Activation code",
                String.format("Hello, %s! Welcome to socialHub. " +
                                "Request for changing password was successful. " +
                                "Please follow the link to activate account: %s/registration/activate/%s",
                        userToUpdate.getUsername(),
                        serverPath,
                        verificationToken)
        );

        userRepository.save(userToUpdate);
    }
}
