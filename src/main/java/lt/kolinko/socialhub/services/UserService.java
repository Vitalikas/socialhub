package lt.kolinko.socialhub.services;

import lt.kolinko.socialhub.dto.UserDto;
import lt.kolinko.socialhub.dto.UserEditDto;
import lt.kolinko.socialhub.dto.UserRegistrationDto;
import lt.kolinko.socialhub.entities.Role;
import lt.kolinko.socialhub.entities.User;
import lt.kolinko.socialhub.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService implements IUserService {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserRepository userRepository;

    @Override
    public List<UserDto> findAll() {
        return userRepository.findAll().stream()
                .map(u -> new UserDto(
                        u.getId(),
                        u.getUsername(),
                        u.getEmail(),
                        u.getPassword(),
                        u.getPicture(),
                        u.isActive(),
                        u.getRoles(),
                        u.getDof(),
                        u.getHomeTown(),
                        u.getLivesIn(),
                        u.getMediaList()))
                .collect(Collectors.toList());
    }

    @Override
    public UserDto findById(Long id) {
        return userRepository.findById(id)
                .map(u -> new UserDto(
                        u.getId(),
                        u.getUsername(),
                        u.getEmail(),
                        u.getPassword(),
                        u.getPicture(),
                        u.isActive(),
                        u.getRoles(),
                        u.getDof(),
                        u.getHomeTown(),
                        u.getLivesIn(),
                        u.getMediaList()))
                .orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find record with ID: %d", id)));
    }

    @Override
    public UserDto findByEmail(String email) {
        User u = userRepository.findByEmail(email);
        UserDto userDto = null;
        if (u != null) {
            userDto = new UserDto(
                    u.getId(),
                    u.getUsername(),
                    u.getEmail(),
                    u.getPassword(),
                    u.getPicture(),
                    u.isActive(),
                    u.getRoles(),
                    u.getDof(),
                    u.getHomeTown(),
                    u.getLivesIn(),
                    u.getMediaList());
        }
        return userDto;
    }

    @Override
    public void save(UserRegistrationDto userRegistrationDto) {
        User user = new User();
        user.setUsername(userRegistrationDto.getUsername());
        user.setEmail(userRegistrationDto.getEmail());
        user.setPassword(passwordEncoder.encode(userRegistrationDto.getPassword()));
        user.setPicture(userRegistrationDto.getPicture());
        user.setActive(false);
        user.setRoles(Collections.singleton(Role.USER));
        user.setLivesIn(userRegistrationDto.getLivesIn());
        user.setVerificationToken(UUID.randomUUID().toString());
        userRepository.save(user);
    }

    @Override
    public void ban(Long id) {
        User userToBan = userRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find record with ID: %d", id)));
        // code here
        userRepository.save(userToBan);
    }

    @Override
    public void update(Long id, UserEditDto userEditDto) {
        User userToUpdate = userRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find record with ID: %d", id)));
        userToUpdate.setUsername(userEditDto.getUsername());
        userToUpdate.setRoles(userEditDto.getRoles());
        userRepository.save(userToUpdate);
    }

    @Override
    public void delete(Long id) {
        userRepository.deleteById(id);
    }

    public boolean activateUser(String token) {
        User user = userRepository.findByVerificationToken(token);

        if (user == null) {
            return false;
        }

        user.setVerificationToken(null);
        user.setActive(true);

        userRepository.save(user);

        return true;
    }
}
