package lt.kolinko.socialhub.utils;

import java.io.File;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class FileDeleteUtil {
    public static boolean deleteAll(List<String> filenames, String uploadPath) {
        AtomicBoolean deleted = new AtomicBoolean(false);
        filenames.forEach(filename -> {
            File f = new File(uploadPath + filename);
            if (f.exists()) {
                f.delete();
                deleted.set(true);
            }
        });
        return deleted.get();
    }
}
