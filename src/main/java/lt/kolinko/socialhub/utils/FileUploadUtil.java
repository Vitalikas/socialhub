package lt.kolinko.socialhub.utils;

import lombok.SneakyThrows;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class FileUploadUtil {
    public static void createUploadDir(String uploadPath) {
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }
    }

    public static String getUuidFilename1(List<MultipartFile> files) {
        String uuidFile = UUID.randomUUID().toString();
        return uuidFile + "." + files.get(0).getOriginalFilename();
    }

    public static String getUuidFilename2(List<MultipartFile> files) {
        String uuidFile = UUID.randomUUID().toString();
        return uuidFile + "." + files.get(1).getOriginalFilename();
    }

    public static String getUuidFilename3(List<MultipartFile> files) {
        String uuidFile = UUID.randomUUID().toString();
        return uuidFile + "." + files.get(2).getOriginalFilename();
    }

    @SneakyThrows
    public static boolean upload1(String uploadPath, String filename, List<MultipartFile> files) {
        boolean uploaded = false;

        MultipartFile file1 = files.get(0);
        if (!file1.isEmpty()) {
            // transfer the uploaded file data to a java.io.File which can be passed between layers
            file1.transferTo(new File( uploadPath + File.separator + filename));
            uploaded = true;
        }
        return uploaded;
    }

    @SneakyThrows
    public static boolean upload2(String uploadPath, String filename, List<MultipartFile> files) {
        boolean uploaded = false;

        MultipartFile file2 = files.get(1);
        if (!file2.isEmpty()) {
            // transfer the uploaded file data to a java.io.File which can be passed between layers
            file2.transferTo(new File( uploadPath + File.separator + filename));
            uploaded = true;
        }
        return uploaded;
    }

    @SneakyThrows
    public static boolean upload3(String uploadPath, String filename, List<MultipartFile> files) {
        boolean uploaded = false;

        MultipartFile file3 = files.get(2);
        if (!file3.isEmpty()) {
            // transfer the uploaded file data to a java.io.File which can be passed between layers
            file3.transferTo(new File( uploadPath + File.separator + filename));
            uploaded = true;
        }
        return uploaded;
    }

    @SneakyThrows
    public static boolean uploadAll(String uploadPath, List<String> filenames, List<MultipartFile> files) {
        boolean uploaded = false;
        if (!files.isEmpty()) {
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }

            AtomicInteger index = new AtomicInteger(0);
            files.forEach(file -> {
                try {
                    file.transferTo(new File( uploadPath + File.separator + filenames.get(index.get())));
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
                index.getAndIncrement();
            });
            uploaded = true;
        }
        return uploaded;
    }

    public static List<String> getUuidFilenames(List<MultipartFile> files) {
        return files.stream()
                .map(file -> UUID.randomUUID().toString() + "." + file.getOriginalFilename())
                .collect(Collectors.toList());
    }

    public static String getUuidFilename(MultipartFile file) {
        String uuidFile = UUID.randomUUID().toString();
        return uuidFile + "." + file.getOriginalFilename();
    }

    @SneakyThrows
    public static boolean upload(String uploadPath, String filename, MultipartFile file) {
        boolean uploaded = false;

        if (!file.isEmpty()) {
            // transfer the uploaded file data to a java.io.File which can be passed between layers
            file.transferTo(new File( uploadPath + File.separator + filename));
            uploaded = true;
        }
        return uploaded;
    }
}
