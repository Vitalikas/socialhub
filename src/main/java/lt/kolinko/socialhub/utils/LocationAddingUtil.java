package lt.kolinko.socialhub.utils;

import lt.kolinko.socialhub.entities.Location;
import java.util.List;

public class LocationAddingUtil {
    public static List<Location> savedLocations() {
        return List.of(
                new Location(new double[]{21.144279, 55.703297}, "Klaipeda"),
                new Location(new double[]{23.903597, 54.898521}, "Kaunas"),
                new Location(new double[]{25.279652, 54.687157}, "Vilnius")
        );
    }

    public static List<Location> getGeolocation(Double longitude, Double latitude) {
        return List.of(new Location(new double[]{longitude, latitude}, "you are here"));
    }

    public static Location getLocation(Double longitude, Double latitude) {
        return new Location(new double[]{latitude, longitude}, "you are here");
    }
}
