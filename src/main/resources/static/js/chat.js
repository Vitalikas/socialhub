'use strict'

let stompClient
let username

const connect = (event) => {
    username = document.querySelector('#username').value.trim()

    if (username) {
        const login = document.querySelector('#login')
        login.classList.add('hide')

        const chatPage = document.querySelector('#chat-page')
        chatPage.classList.remove('hide')

        const socket = new SockJS('/chat')
        stompClient = Stomp.over(socket)
        stompClient.connect({}, onConnected, onError)
    }
    event.preventDefault()
}

const onConnected = () => {
    stompClient.subscribe('/topic/public', onMessageReceived)
    stompClient.send("/app/chat.newUser",
        {},
        JSON.stringify({sender: username, type: 'CONNECT'})
    )
    const status = document.querySelector('#status')
    status.className = 'hide'
}

const onError = (error) => {
    const status = document.querySelector('#status')
    status.innerHTML = 'Could not find the connection you were looking for. Move along or refresh the page!'
    status.style.color = 'red'
}

const sendMessage = (event) => {
    const messageInput = document.querySelector('#message')
    const messageContent = messageInput.value.trim()

    if (messageContent && stompClient) {
        const chatMessage = {
            sender: username,
            content: messageInput.value,
            type: 'CHAT',
            time: moment().format("YYYY-MM-DD HH:mm:ss")
        }
        stompClient.send("/app/chat.send", {}, JSON.stringify(chatMessage))
        messageInput.value = ''
    }
    event.preventDefault();
}

const onMessageReceived = (payload) => {
    const message = JSON.parse(payload.body);

    const chatCard = document.createElement('div')
    chatCard.className = 'card-body'

    const record = document.createElement('div')
    record.className = 'd-flex justify-content-end mb-2'
    chatCard.appendChild(record)

    const stamp = document.createElement('div')
    stamp.className = 'd-flex justify-content-end mb-4'
    chatCard.appendChild(stamp)

    // creating message container 'messageContainer'
    const messageContainer = document.createElement('div')
    messageContainer.className = 'msg_container_send'

    record.appendChild(messageContainer)

    if (message.type === 'CONNECT') {
        messageContainer.classList.add('event-message')
        message.content = message.sender + ' connected'
    } else if (message.type === 'DISCONNECT') {
        messageContainer.classList.add('event-message')
        message.content = message.sender + ' left the chat'
    } else {
        messageContainer.style['background-color'] = getAvatarColor(message.sender)

        // creating avatar container 'avatarContainer'
        const avatarContainer = document.createElement('div')
        avatarContainer.className = 'avatar_container_send'

        // creating avatar element (circle) 'avatarElement'
        const avatarElement = document.createElement('div')
        avatarElement.className = 'avatar_element_send'
        avatarElement.style['background-color'] = getAvatarColor(message.sender)

        // creating avatar text (username first char) 'avatarText'
        const avatarText = document.createTextNode(message.sender[0])

        // creating timestamp container 'timestampContainer
        const timestampContainer = document.createElement('div')
        timestampContainer.className = 'timestamp_container_send'
        timestampContainer.innerHTML = message.time

        avatarElement.appendChild(avatarText);
        avatarContainer.appendChild(avatarElement)

        record.appendChild(avatarContainer)
        stamp.appendChild(timestampContainer)
    }

    messageContainer.innerHTML = message.content

    const chat = document.querySelector('#chat')
    chat.appendChild(record)
    chat.appendChild(stamp)
    chat.scrollTop = chat.scrollHeight
}

const hashCode = (str) => {
    let hash = 0
    for (let i = 0; i < str.length; i++) {
       hash = str.charCodeAt(i) + ((hash << 5) - hash)
    }
    return hash
}

const getAvatarColor = (messageSender) => {
    const colours = ['#2196F3', '#32c787', '#1BC6B4', '#A1B4C4']
    const index = Math.abs(hashCode(messageSender) % colours.length)
    console.log(index)
    return colours[index]
}

const loginForm = document.querySelector('#login-form')
loginForm.addEventListener('submit', connect, true)
const messageControls = document.querySelector('#message-controls')
messageControls.addEventListener('submit', sendMessage, true)