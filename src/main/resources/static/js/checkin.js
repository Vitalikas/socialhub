function initMap(mapId, latlng) {
    let geocoder = new google.maps.Geocoder();
    let marker = new google.maps.Marker();

    geocoder.geocode({ location: latlng }, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
            let mapOptions = {
                zoom: 9,
                center: results[0].geometry.location,
                disableDefaultUI: false
            };

            let map = new google.maps.Map(document.getElementById(mapId), mapOptions);

            let marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });

            let infowindow = new google.maps.InfoWindow();
            infowindow.setContent(results[0].formatted_address);
            infowindow.open(map, marker);

        } else {
            window.alert("Geocoder failed due to: " + status);
        }
    });
}
