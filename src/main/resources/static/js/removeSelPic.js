function removeProfileImg() {
    var file = document.getElementById("profile-img");
    file.value = file.defaultValue;

    var preview = document.getElementById("prv-profile-img");
    preview.style.display = "none";

    var button = document.getElementById("rmv-profile-img");
    button.style.display = "none";
}

function remove1() {
    var file1 = document.getElementById("file1");
    file1.value = file1.defaultValue;

    var preview1 = document.getElementById("preview1");
    preview1.style.display = "none";

    var button1 = document.getElementById("rmv1");
    button1.style.display = "none";
}

function remove2() {
    var file2 = document.getElementById("file2");
    file2.value = file2.defaultValue;

    var preview2 = document.getElementById("preview2");
    preview2.style.display = "none";

    var button2 = document.getElementById("rmv2");
    button2.style.display = "none";
}

function remove3() {
    var file3 = document.getElementById("file3");
    file3.value = file3.defaultValue;

    var preview3 = document.getElementById("preview3");
    preview3.style.display = "none";

    var button3 = document.getElementById("rmv3");
    button3.style.display = "none";
}
